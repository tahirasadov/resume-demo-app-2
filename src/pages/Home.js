import React from 'react';

import { Link } from "react-router-dom";

class Home extends React.Component {

  render() {

    return (
      <React.Fragment>
        <section className="section">
          <div className="container">
            <div className="columns is-centered is-vcentered">
              <div className="column is-half has-text-centered">
                <h1 className="title">
                  Create your online resume
                </h1>
                <Link className="button is-primary is-warning" to="/login">Login</Link>
                {" "}
                <Link className="button is-primary is-success" to="/register">Register</Link>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    );

  }

}

export default Home;