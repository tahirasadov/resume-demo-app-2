import React from 'react';

import { AppContext } from './../contexts/AppContext';

//Router
import { withRouter } from "react-router-dom";

class Register extends React.Component {


  static contextType = AppContext;

  constructor( props ) {

    super( props );
    
    this.state = {
      registerBtnText: 'Register',
      hasError: false,
      hasSuccess: false,
      errors: {},
      email: '',
      password: ''
    }

    this.register = this.register.bind( this );
    this.handleInput = this.handleInput.bind( this );
    this.handleInput = this.handleInput.bind( this );

  }


  handleInput( e ) {

    this.setState( {
      [ e.target.name ]: e.target.value
    } );
    
  }

  componentDidMount() {
    
    const { setToken } = this.context;
  
    setToken( '' );
    
  }
  

  register( e ) {
    
    e.preventDefault();

    this.setState( { registerBtnText: 'Registering...' } );

    fetch( process.env.REACT_APP_API_URL + '/register', {

      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: this.state.email,
        password: this.state.password,
      })

    } )
    .then( result => {
      return result.json();
    })
    .then( response => {
      this.setState( { registerBtnText: 'Register' } );
      if( response.status === 'ok' ){
                
        this.setState( {
          hasError: false,
          hasSuccess: true,
          message: response.message,
          errors: {},
          email: '',
          password: '',
        } )

      }else if( response.status === 'error' ){
        this.setState( { 
          hasError: true,
          hasSuccess: false,
          message: response.message,
          errors: response.errors,
        } )
      }else {
        this.setState( { hasError: true, errors: response.errors, message: response.message } )
      }
      
    } );
  }

  render() {

    return (
      <React.Fragment>
        <section className="section">
          <div className="container">
            <div className="columns is-centered is-vcentered">
              <div className="column is-one-quarter has-text-centered">
                <h1 className="title">
                  Register
                </h1>

                { this.state.hasError && <div className="notification is-danger">
                  { this.state.message }
                </div>
                }
                { this.state.hasSuccess && <div className="notification is-success">
                  { this.state.message }
                </div>
                }

                <form onSubmit={ this.register } action="" method="POST" noValidate>
                  <div className="field">
                    <div className="control has-icons-left has-icons-right">
                      <input value={ this.state.email } onChange={ this.handleInput } className={ this.state.errors && this.state.errors.email ? 'input is-danger' : 'input' } name="email" type="email" placeholder="Email" />
                      <span className="icon is-small is-left">
                        <i className="fas fa-envelope"></i>
                      </span>
                    </div>
                    { this.state.errors && this.state.errors.email && <p className="help is-danger">{ this.state.errors.email }</p> }
                  </div>


                  <div className="field">
                    <div className="control has-icons-left has-icons-right">
                      <input value={ this.state.password } onChange={ this.handleInput } className={ this.state.errors && this.state.errors.password ? 'input is-danger' : 'input' } name="password" type="password" placeholder="Password" />
                      <span className="icon is-small is-left">
                        <i className="fas fa-lock"></i>
                      </span>
                    </div>
                    { this.state.errors && this.state.errors.password && <p className="help is-danger">{ this.state.errors.password }</p> }
                  </div>

                  <div className="control">
                    <button className="button is-primary">{ this.state.registerBtnText }</button>
                  </div>

                </form>
              </div>
            </div>
            
          </div>
        </section>
      </React.Fragment>
    );

  }

}

export default withRouter( Register );