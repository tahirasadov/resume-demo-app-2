import React from 'react';

//Router
import { withRouter } from "react-router-dom";

import { AppContext } from './../contexts/AppContext';

import ViewCard from './../components/ViewCard';
import UserNotFound from './../components/UserNotFound';
import ViewLoading from './../components/ViewLoading';

class View extends React.Component {

  static contextType = AppContext;

  constructor( props ) {
    
    super( props );
    
    this.state = {
      username: '',
      loading: true,
      info: false
    }
    
  }

  componentDidMount() {

    let username = this.props.match.params.username;
    this.setState( { username: username } );

    const { getUserInfo } = this.context;

    getUserInfo( username )
    .then( doc => {
      this.setState( { loading: false, info: doc.info } );
    } )
    .catch( error => {
      this.setState( { loading: false, info: false } );
      console.log( error );
    } );

  }
  

  render() {

    return (
      <React.Fragment>
      { this.state.loading && <ViewLoading username={ this.state.username } /> }

      { !this.state.loading && this.state.info ? <ViewCard info={ this.state.info } /> : <UserNotFound /> }

      </React.Fragment>
    );

  }

}

export default withRouter( View );