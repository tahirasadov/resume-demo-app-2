import React from 'react';

import { AppContext } from './../contexts/AppContext';

import ProfileForm from './../components/ProfileForm';
import Error from './../components/Error';

//Router
// import { withRouter } from "react-router-dom";

class Profile extends React.Component {

  static contextType = AppContext;

  constructor( props ) {
    
    super( props );

    this.state = {
      fullname: '',
      username: '',
      jobtitle: '',
      website: '',
      behance: '',
      dribbble: '',
      facebook: '',
      email: '',
      phone: '',
      description: '',
      avatar: false,
      errors: {},
      updateButtonText: 'Update',
      loading: true,
      info: false
    }

    this.handleInput = this.handleInput.bind( this );
    this.onSubmit = this.onSubmit.bind( this );
    this.uploadAvatar = this.uploadAvatar.bind( this );

  }

  componentDidMount() {

    const { getUserInfo } = this.context;
    console.log(this.state);
    
    getUserInfo()
    .then( doc => {
      this.setState( { loading: false, ...doc.info, info: doc.info } , () => {
        
    console.log(this.state);
      });
    } )
    .catch( error => {
      this.setState( { loading: false, info: false } , () => {
            console.log(this.state);
      });
      console.log( error );
    } );
      
  }
  

  
  handleInput( e ) {

    this.setState( {
      [ e.target.name ]: e.target.value
    } );

    if( e.target.name === 'avatar' ){
      this.setState( {
        [ e.target.name ]: e.target.files[ 0 ]
      } );
    }
    
  }

  uploadAvatar( file ) {
    
    this.setState( { updateButtonText: 'Uploading profile image...' } );

    const { state } = this.context;

    let formData = new FormData();

    formData.append( 'file', file );
    formData.append( 'Authorization', 'Bearer ' + state.token );
    
    fetch( process.env.REACT_APP_API_URL + '/upload', {
      method: 'POST',
      body: formData,
    } )
    .then( data => {
      return data.json();
    } )
    .then( response => {
      
      this.setState( { updateButtonText: 'Update' } );

      if( response.status === 'error' ){
        this.setState( {
          errors: { avatar: response.message }
        } );
      }else {
        window.open( '/@' + this.state.username, "_blank");
      }

    } )
    .catch( error => {
      this.setState( { updateButtonText: 'Update' } );
      console.log(error);
    } );
    
  }

  onSubmit( e ) {

    e.preventDefault();
    
    const { state } = this.context;

    this.setState( { errors: {}, updateButtonText: 'Updating...' } );

    fetch( process.env.REACT_APP_API_URL + '/update_profile', {

      method: 'POST',
      headers: {
        'Authorization': 'Bearer ' + state.token,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        fullname: this.state.fullname,
        username: this.state.username,
        jobtitle: this.state.jobtitle,
        website: this.state.website,
        behance: this.state.behance,
        dribbble: this.state.dribbble,
        facebook: this.state.facebook,
        email: this.state.email,
        phone: this.state.phone,
        description: this.state.description,
      })

    } )
    .then( result => {
      return result.json();
    })
    .then( response => {
      this.setState( { updateButtonText: 'Update' } );
      if( response.status === 'ok' ){
        
        
        if( this.state.avatar ){
          this.uploadAvatar( this.state.avatar );
        }else {
          this.setState( {
            hasError: false,
            errors: {}
          } )
  
          window.open( '/@' + this.state.username, "_blank");
        }

      }else {
        this.setState( { errors: response.errors, errorMessage: response.message } )
      }
      
    } );

    
  }
  

  render() {
    const { state } = this.context;
    
    return (
      <React.Fragment>
        { state.token ? (
           <ProfileForm errors={ this.state.errors } state={ this.state } handleInput={ this.handleInput } onSubmit={ this.onSubmit } />
          ) : (
          <Error />
        ) }
      </React.Fragment>
    );

  }

}

export default Profile;