import React from 'react';

//Router
import { withRouter } from "react-router-dom";

export const AppContext = React.createContext();

class AppContextProvider extends React.Component {

  constructor( props ) {
    super( props );

    this.state = {
      token: false,
    }
    
    this.setToken = this.setToken.bind( this );
    this.getCookie = this.getCookie.bind( this );
    this.setCookie = this.setCookie.bind( this );
    this.getUserInfo = this.getUserInfo.bind( this );
  }

  setToken( token ) {
    this.setState( { token: token } );
    this.setCookie( token );
  }

  componentDidMount() {
    let token = this.getCookie( 'firebasetoken' );
    this.setState( { token: token } );
    
  }
  

  getCookie( cookieName ) {
    let name = cookieName + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }



  setCookie( token ) {
    var d = new Date();
    // Expires in a day
    d.setTime(d.getTime() + (24*60*60*1000));
    let expires = "expires="+ d.toUTCString();
    document.cookie = "firebasetoken=" + token + ";" + expires + ";path=/";
  }

  getUserInfo( username = false ) {

    return fetch( process.env.REACT_APP_API_URL + '/profile', {

      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.getCookie( 'firebasetoken' ),
      },
      body: JSON.stringify({
        username: username,
      })

    } )
    .then( data => data.json() )
    .then( response => {
      return { loading: false, info: response.info };
      
    } )
    .catch( error => {
      return { loading: false, info: false };
      
    } )

  }

  render() {

    return(
      <AppContext.Provider value={ { state: this.state, setToken: this.setToken, getUserInfo: this.getUserInfo } }>
        { this.props.children }
      </AppContext.Provider>
    );

  }

}

export default withRouter( AppContextProvider );