import React from 'react';


class ProfileForm extends React.Component {

  render() {

    return (
      <React.Fragment>
        <section className="section">
          <div className="container">
            <div className="columns is-centered is-vcentered">
              <div className="column is-half has-text-centered">
                <h1 className="title">
                  Update your profile
                </h1>
                <form action="" method="POST" onSubmit={ this.props.onSubmit }>
                  <div className="field is-horizontal">
                    <div className="field-label is-normal">
                      <label className="label">Personal</label>
                    </div>

                    <div className="field-body">

                      <div className="field">
                        <p className="control is-expanded has-icons-left">
                          <input  onChange={ this.props.handleInput }  value={ this.props.state.fullname } name="fullname" className={ this.props.errors && this.props.errors.fullname ? 'input is-danger' : 'input' }  type="text" placeholder="Fullname" />
                          <span className="icon is-small is-left">
                            <i className="fas fa-user"></i>
                          </span>
                        </p>
                        { this.props.errors && this.props.errors.fullname && <p className="help is-danger">{ this.props.errors.fullname }</p> }
                      </div>

                      <div className="field">
                        <p className="control is-expanded has-icons-left has-icons-right">
                          <input  onChange={ this.props.handleInput }  value={ this.props.state.username } name="username"  className={ this.props.errors && this.props.errors.username ? 'input is-danger' : 'input' } type="text" placeholder="Username" />
                          <span className="icon is-small is-left">
                            <i className="fas fa-at"></i>
                          </span>
                        </p>
                        { this.props.errors && this.props.errors.username && <p className="help is-danger">{ this.props.errors.username }</p> }
                      </div>

                    </div>
                  </div>
                  
                  <div className="field is-horizontal">
                    <div className="field-label"></div>
                    <div className="field-body">

                      <div className="field is-expanded">
                        <div className="field">
                          <p className="control is-expanded has-icons-left has-icons-right">
                            <input  onChange={ this.props.handleInput }  value={ this.props.state.jobtitle } name="jobtitle" className={ this.props.errors && this.props.errors.jobtitle ? 'input is-danger' : 'input' }  type="text" placeholder="Jobtitle" />
                            <span className="icon is-small is-left">
                              <i className="fas fa-address-card"></i>
                            </span>
                          </p>
                          { this.props.errors && this.props.errors.jobtitle && <p className="help is-danger">{ this.props.errors.jobtitle }</p> }
                        </div>
                      </div>

                    </div>

                  </div>


                  
                  
                  <div className="field is-horizontal">
                    <div className="field-label is-normal">
                      <label className="label">Portflio</label>
                    </div>
                    <div className="field-body">
                      <div className="field is-expanded">
                        <div className="field">
                          <p className="control is-expanded has-icons-left has-icons-right">
                            <input  onChange={ this.props.handleInput }  value={ this.props.state.website } name="website" className={ this.props.errors && this.props.errors.website ? 'input is-danger' : 'input' }  type="text" placeholder="Website" />
                            <span className="icon is-small is-left">
                            <i className="fas fa-globe"></i>
                            </span>
                          </p>
                          { this.props.errors && this.props.errors.website && <p className="help is-danger">{ this.props.errors.website }</p> }
                        </div>
                      </div>
                    </div>
                  </div>

                  
                  <div className="field is-horizontal">
                    <div className="field-label is-normal">
                      <label className="label"></label>
                    </div>
                    <div className="field-body">
                      <div className="field is-expanded">
                        <div className="field">
                          <p className="control is-expanded has-icons-left has-icons-right">
                            <input  onChange={ this.props.handleInput }  value={ this.props.state.behance } name="behance" className={ this.props.errors && this.props.errors.behance ? 'input is-danger' : 'input' }  type="text" placeholder="Behance" />
                            <span className="icon is-small is-left">
                              <i className="fab fa-behance"></i>
                            </span>
                          </p>
                          { this.props.errors && this.props.errors.behance && <p className="help is-danger">{ this.props.errors.behance }</p> }
                        </div>
                      </div>
                    </div>
                  </div>

                  
                  
                  <div className="field is-horizontal">
                    <div className="field-label is-normal">
                      <label className="label"></label>
                    </div>
                    <div className="field-body">
                      <div className="field is-expanded">
                        <div className="field">
                          <p className="control is-expanded has-icons-left has-icons-right">
                            <input  onChange={ this.props.handleInput }  value={ this.props.state.dribbble } name="dribbble" className={ this.props.errors && this.props.errors.dribbble ? 'input is-danger' : 'input' }  type="text" placeholder="Dribbble" />
                            <span className="icon is-small is-left">
                              <i className="fab fa-dribbble"></i>
                            </span>
                          </p>
                          { this.props.errors && this.props.errors.dribbble && <p className="help is-danger">{ this.props.errors.dribbble }</p> }
                        </div>
                      </div>
                    </div>
                  </div>

                  
                  

                  
                  
                  <div className="field is-horizontal">
                    <div className="field-label is-normal">
                      <label className="label"></label>
                    </div>
                    <div className="field-body">
                      <div className="field is-expanded">
                        <div className="field">
                          <p className="control is-expanded has-icons-left has-icons-right">
                            <input  onChange={ this.props.handleInput }  value={ this.props.state.facebook } name="facebook" className={ this.props.errors && this.props.errors.facebook ? 'input is-danger' : 'input' }  type="text" placeholder="Facebook" />
                            <span className="icon is-small is-left">
                              <i className="fab fa-facebook"></i>
                            </span>
                          </p>
                          { this.props.errors && this.props.errors.facebook && <p className="help is-danger">{ this.props.errors.facebook }</p> }
                        </div>
                      </div>
                    </div>
                  </div>

                  
                  
                  <div className="field is-horizontal">
                    <div className="field-label is-normal">
                      <label className="label">Contact</label>
                    </div>

                    <div className="field-body">

                      <div className="field">
                        <p className="control is-expanded has-icons-left">
                          <input  onChange={ this.props.handleInput }  value={ this.props.state.email } name="email" className={ this.props.errors && this.props.errors.email ? 'input is-danger' : 'input' }  type="email" placeholder="E-mail" />
                          <span className="icon is-small is-left">
                            <i className="fas fa-user"></i>
                          </span>
                        </p>
                        { this.props.errors && this.props.errors.email && <p className="help is-danger">{ this.props.errors.email }</p> }
                      </div>

                      <div className="field">
                        <p className="control is-expanded has-icons-left has-icons-right">
                          <input  onChange={ this.props.handleInput }  value={ this.props.state.phone } name="phone" className={ this.props.errors && this.props.errors.phone ? 'input is-danger' : 'input' }  type="text" placeholder="Phone" />
                          <span className="icon is-small is-left">
                          <i className="fas fa-phone"></i>
                          </span>
                        </p>
                        { this.props.errors && this.props.errors.phone && <p className="help is-danger">{ this.props.errors.phone }</p> }
                      </div>

                    </div>
                  </div>
                  
                  
                  <div className="field is-horizontal">
                    <div className="field-label is-normal">
                      <label className="label">Description</label>
                    </div>
                    <div className="field-body">
                      <div className="field">
                        <div className="control">
                          <textarea value={ this.props.state.description } onChange={ this.props.handleInput }  name="description"  className={ this.props.errors && this.props.errors.facebook ? 'input is-danger' : 'textarea' } placeholder="Explain how we can help you"></textarea>
                        </div>
                        { this.props.errors && this.props.errors.description && <p className="help is-danger">{ this.props.errors.description }</p> }
                      </div>
                    </div>
                  </div>
                  
                  
                  <div className="field is-horizontal">
                    <div className="field-label is-normal">
                      <label className="label">Description</label>
                    </div>
                    <div className="field-body">
                      <div className="field">
                        <div className="control">
                          <input  onChange={ this.props.handleInput } name="avatar" className={ this.props.errors && this.props.errors.avatar ? 'input is-danger' : 'input' }  type="file" />
                        </div>
                        { this.props.errors && this.props.errors.avatar && <p className="help is-danger">{ this.props.errors.avatar }</p> }
                      </div>
                    </div>
                  </div>
                  
                  <div className="field is-horizontal">
                    <div className="field-label">
                      { " " }
                    </div>
                    <div className="field-body">
                      <div className="field">
                        <div className="control">
                          <button className="button is-primary">
                            { this.props.state.updateButtonText }
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>

                </form>
              </div>
            </div>
            
          </div>
        </section>
      </React.Fragment>
    );

  }

}

export default ProfileForm;