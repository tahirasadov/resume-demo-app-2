import React from 'react';
const Error = () => {
  return (
    <section className="section">
      <div className="container">
        <div className="columns is-centered is-vcentered">
          <div className="column is-half has-text-centered">
            <div className="notification is-danger">Please login</div>
          </div>
        </div>
      </div>
    </section>
    )
};
export default Error;