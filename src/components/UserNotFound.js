import React from 'react';
const UserNotFound = () => {
  return (
    <section className="section">
      <div className="container">
        <div className="columns is-centered is-vcentered">
          <div className="column is-half has-text-centered">
            <div className="notification is-danger">User not found</div>
          </div>
        </div>
      </div>
    </section>
    )
};
export default UserNotFound;