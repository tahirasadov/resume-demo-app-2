import React from 'react';


import './../View.css';

const ViewCard = ( props ) => {

  return(
    <React.Fragment>
        <section className="section">
          <div className="container">
            <div className="columns is-centered is-vcentered">
              <div className="column is-half has-text-centered">
                { <figure className="image is-128x128"><img src={ props.info.avatar } alt="Avatar" /></figure> }
                <br />
                <h1 className="title">{ props.info.fullname }</h1>
                <h2 className="subtitle">{ props.info.jobtitle }</h2>
                <p className="description">{ props.info.description }</p>
                <hr />
                <h2 className="subtitle">Social links</h2>
                <div className="social-links">

                  { props.info.website && (
                  <a href={ props.info.website } className="website" title="Website">
                    <span className="icon">
                      <i className="fas fa-globe"></i>
                    </span>
                    Website
                  </a>
                  ) }

                  { props.info.behance && (
                  <a href={ props.info.behance } className="behance" title="Behance">
                    <span className="icon">
                      <i className="fab fa-behance"></i>
                    </span>
                    Behance
                  </a>
                  ) }
                  
                  { props.info.dribbble && (
                  <a href={ props.info.dribbble } className="dribbble" title="Dribbble">
                    <span className="icon">
                      <i className="fas fa-basketball-ball"></i>
                    </span>
                    Dribbble
                  </a>
                  ) }

                  { props.info.facebook && (
                  <a href={ props.info.facebook } className="facebok" title="Facebok">
                    <span className="icon">
                      <i className="fab fa-facebook-f"></i>
                    </span>
                    Facebok
                  </a>
                  ) }
                </div>
                <hr />
                <h2 className="subtitle">Contact</h2>
                <div className="contact-info">
                  { props.info.email && (
                  <a href={ 'mailto:' + props.info.email } className="email" title="E-mail">
                    <span className="icon">
                      <i className="far fa-envelope"></i>
                    </span>
                    { props.info.email }
                  </a>
                  ) }


                  { props.info.phone && (
                  <span className="phone" title="Phone number">
                    <span className="icon">
                      <i className="fas fa-mobile"></i>
                    </span>
                    { props.info.phone }
                  </span>
                  ) }
                </div>
              </div>
            </div>
            
          </div>
        </section>
      </React.Fragment>
  );

}

export default ViewCard;