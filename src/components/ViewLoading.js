import React from 'react';


import './../ViewLoading.css';

const ViewLoading = ( props ) => {

  return(
    <React.Fragment>
        <section className="section">
          <div className="container">
            <div className="columns is-centered is-vcentered">
              <div className="column is-half has-text-centered">
              <div className="spinner">
                <div className="bounce1"></div>
                <div className="bounce2"></div>
                <div className="bounce3"></div>
              </div>
                { props.username && <b>@{props.username}</b> }
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
  );

}

export default ViewLoading;