import React from 'react';

// Pages
import Home     from './pages/Home';
import Register from './pages/Register';
import Login    from './pages/Login';
import Profile  from './pages/Profile';
import View     from './pages/View';

// Context
import AppContextProvider from './contexts/AppContext';

// Router
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import './App.css';

class App extends React.Component {

  state = {
    token: '',
  }

  componentDidMount() {
    this.setState( {
      token: 'token'
    } );
  }
  

  login(){

  }

  render() {

    return (
      <Router>
        <AppContextProvider>
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route exact path="/login">
              <Login />
            </Route>
            <Route exact path="/register">
              <Register />
            </Route>
            <Route exact path="/profile">
              <Profile />
            </Route>
            <Route exact path="/@:username" component={ View } />
          </Switch>
        </AppContextProvider>
      </Router>
    );

  }

}

export default App;
